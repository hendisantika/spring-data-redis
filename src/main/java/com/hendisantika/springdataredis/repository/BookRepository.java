package com.hendisantika.springdataredis.repository;

import com.hendisantika.springdataredis.domain.Book;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-data-redis
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-26
 * Time: 14:09
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class BookRepository {

    @Inject
    private RedisTemplate<String, Book> redisTemplate;

    public void save(Book book) {
        redisTemplate.opsForValue().set(book.getId(), book);
    }

    public Book findById(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();

        Set<String> keys = redisTemplate.keys("*");
        Iterator<String> it = keys.iterator();

        while (it.hasNext()) {
            books.add(findById(it.next()));
        }

        return books;
    }

    public void delete(Book b) {
        String key = b.getId();
        redisTemplate.opsForValue().getOperations().delete(key);
    }


    public void deleteAll() {
        Set<String> keys = redisTemplate.keys("*");
        Iterator<String> it = keys.iterator();

        while (it.hasNext()) {
            Book b = new Book(it.next());
            delete(b);
        }
    }
}