package com.hendisantika.springdataredis.util;

import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-data-redis
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-26
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
public class UrlUtil {
    public static String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {

        String enc = httpServletRequest.getCharacterEncoding();

        if (enc == null)
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;

        pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        return pathSegment;
    }
}
