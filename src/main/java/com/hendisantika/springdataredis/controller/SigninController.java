package com.hendisantika.springdataredis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-data-redis
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-26
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class SigninController {
    @RequestMapping(value = "signin")
    public String signin() {
        return "signin/signin";
    }
}